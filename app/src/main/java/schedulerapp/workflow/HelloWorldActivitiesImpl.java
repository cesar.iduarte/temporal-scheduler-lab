package schedulerapp.workflow;

public class HelloWorldActivitiesImpl implements HelloWorldActivities {
    @Override
    public String composeGreeting(String name) {
        return "Hello " + name + "!";
    }
}
