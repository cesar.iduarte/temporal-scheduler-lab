package schedulerapp.workflow;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;


@ActivityInterface
public interface HelloWorldActivities {
    @ActivityMethod(name="composeGreeting")
    String composeGreeting(String name);
}
