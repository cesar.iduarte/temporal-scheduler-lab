package schedulerapp.workflow;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;

import java.time.Duration;

import org.slf4j.Logger;


public class HelloWorldWorkflowImp implements HelloWorldWorkflow {
    ActivityOptions options = ActivityOptions.newBuilder()
    .setStartToCloseTimeout(Duration.ofSeconds(60))
    .build();


    private final HelloWorldActivities activity = Workflow.newActivityStub(HelloWorldActivities.class, options);
    private Logger logger = Workflow.getLogger(this.getClass().getName());

    @Override
    public String getGreeting(String name) {

        logger.info("My Id: " + Workflow.getInfo().getWorkflowId());
        logger.info("My runId: " + Workflow.getInfo().getRunId());

        Workflow.sleep(Duration.ofSeconds(5));
        return activity.composeGreeting(name);
    }
}
