package schedulerapp.cmd;

import schedulerapp.domain.SchedulerStatusCode;
import schedulerapp.domain.Responses.SchedulerResponse;
import schedulerapp.services.ScheduleCtrl;


public class ResumeCommandImpl implements Command {
    private final ScheduleCtrl scheduler;
    public  ResumeCommandImpl(ScheduleCtrl scheduler) {
        this.scheduler = scheduler;
    }

    public void execute() {
        System.out.print("Enter schedule ID:\n");
        String scheduleID = System.console().readLine();

        SchedulerResponse resp = scheduler.resume(scheduleID);

         if (resp.status == SchedulerStatusCode.OK) {
            System.out.println("\n\nSchedule resumed...");
           
        } else {
            System.out.println("\n\nSchedule could not be resumed...");
            System.out.println(resp.error);
        }
    }
}
