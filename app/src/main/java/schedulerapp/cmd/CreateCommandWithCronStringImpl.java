package schedulerapp.cmd;

import schedulerapp.Shared;
import schedulerapp.domain.SchedulerOptions;
import schedulerapp.domain.SchedulerStatusCode;
import schedulerapp.domain.Responses.SchedulerResponse;
import schedulerapp.services.ScheduleCtrl;
import schedulerapp.workflow.HelloWorldWorkflow;

public class CreateCommandWithCronStringImpl implements Command {
    private final ScheduleCtrl scheduler;
    public  CreateCommandWithCronStringImpl(ScheduleCtrl scheduler) {
        this.scheduler = scheduler;
    }
    public void execute() {
        System.out.print("Enter schedule ID:\n");
        String scheduleID = System.console().readLine();
        SchedulerOptions options = new SchedulerOptions();

        options.TaskQueue = Shared.HELLO_WORLD_TASK_QUEUE;
        options.WorkflowType = HelloWorldWorkflow.class;
        options.Arguments = "World!";
        options.EntityID = "Machine 1";
        options.CronString = "25 * * * *";

        SchedulerResponse resp = this.scheduler.create(scheduleID, options);

        if (resp.status == SchedulerStatusCode.OK) {
            System.out.printf("\n\nSchedule created: %s\n", resp.scheduleID);
        } else {
            System.out.printf("\n\nSchedule could not be created: %s", resp.error);
        }
    }
}
