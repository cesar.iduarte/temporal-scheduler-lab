package schedulerapp.cmd;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import schedulerapp.Shared;
import schedulerapp.domain.SchedulerOptions;
import schedulerapp.domain.SchedulerStatusCode;
import schedulerapp.domain.Responses.SchedulerResponse;
import schedulerapp.services.ScheduleCtrl;
import schedulerapp.workflow.HelloWorldWorkflow;

public class CreateCommandWithIntervalImpl implements Command {
    private final ScheduleCtrl scheduler;
    public  CreateCommandWithIntervalImpl(ScheduleCtrl scheduler) {
        this.scheduler = scheduler;
    }
    public void execute() {
        System.out.print("Enter schedule ID:\n");
        String scheduleID = System.console().readLine();
        SchedulerOptions options = new SchedulerOptions();

        options.TaskQueue = Shared.HELLO_WORLD_TASK_QUEUE;
        options.WorkflowType = HelloWorldWorkflow.class;
        options.Arguments = "World!";
        options.EntityID = "Machine 1";
        options.Interval = Duration.ofMinutes(15);

        // Search Attributes needs to be configured first in the cluster
        // https://docs.temporal.io/visibility#custom-search-attributes
        /*options.SearchAttributes = new HashMap<>();
        HashMap<String, String> attributes = new HashMap<String, String>();
        attributes.put("customfield1", "1");
        attributes.put("customfield2", "1");
        options.SearchAttributes = attributes;*/

        SchedulerResponse resp = this.scheduler.create(scheduleID, options);

        if (resp.status == SchedulerStatusCode.OK) {
            System.out.printf("\n\nSchedule created: %s\n", resp.scheduleID);
        } else {
            System.out.printf("\n\nSchedule could not be created: %s", resp.error);
        }
    }
}
