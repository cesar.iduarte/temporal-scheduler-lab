package schedulerapp.cmd;

import schedulerapp.domain.SchedulerStatusCode;
import schedulerapp.domain.Responses.SchedulerResponse;
import schedulerapp.services.ScheduleCtrl;

public class PausedCommandImpl implements Command {
    private final ScheduleCtrl scheduler;
    public  PausedCommandImpl(ScheduleCtrl scheduler) {
        this.scheduler = scheduler;
    }

    public void execute() {
        System.out.print("Enter schedule ID:\n");
        String scheduleID = System.console().readLine();

        SchedulerResponse resp = this.scheduler.pause(scheduleID);

        if (resp.status == SchedulerStatusCode.OK) {
            System.out.println("\n\nSchedule paused...");
           
        } else {
            System.out.println("\n\nSchedule could not be paused...");
            System.out.println(resp.error);
        }
    }
}
