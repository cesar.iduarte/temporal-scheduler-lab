package schedulerapp.cmd;

import java.time.Duration;

import schedulerapp.domain.SchedulerOptions;
import schedulerapp.domain.SchedulerStatusCode;
import schedulerapp.domain.Responses.SchedulerResponse;
import schedulerapp.services.ScheduleCtrl;

public class UpdateCommandImpl implements Command {
    private final ScheduleCtrl scheduler;
    public  UpdateCommandImpl(ScheduleCtrl scheduler) {
        this.scheduler = scheduler;
    }

    public void execute() {
        System.out.print("Enter schedule ID:\n");
        String scheduleID = System.console().readLine();

        SchedulerOptions opts = new SchedulerOptions();
        opts.Arguments = "One more Time";
        opts.Interval = Duration.ofMinutes(5);

        SchedulerResponse resp = scheduler.update(scheduleID, opts);

        if (resp.status == SchedulerStatusCode.OK) {
            System.out.println("\n\nSchedule was updated...");
           
        } else {
            System.out.println("\n\nSchedule could not be updated...");
            System.out.println(resp.error);
        }
    }
}