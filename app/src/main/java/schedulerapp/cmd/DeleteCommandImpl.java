package schedulerapp.cmd;

import schedulerapp.domain.SchedulerStatusCode;
import schedulerapp.domain.Responses.SchedulerResponse;
import schedulerapp.services.ScheduleCtrl;

public class DeleteCommandImpl implements Command {
    private final ScheduleCtrl scheduler;
    public  DeleteCommandImpl(ScheduleCtrl scheduler) {
        this.scheduler = scheduler;
    }

    public void execute() {
        System.out.print("Enter schedule ID:\n");
        String scheduleID = System.console().readLine();
        SchedulerResponse resp = this.scheduler.delete(scheduleID);

        if (resp.status == SchedulerStatusCode.OK) {
            System.out.println("\n\nSchedule deleted...");   
        } else {
            System.out.println(resp.error);
        }
    }
}
