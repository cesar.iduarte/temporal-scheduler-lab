package schedulerapp.cmd;

import java.util.List;

import schedulerapp.domain.SchedulerListEntry;
import schedulerapp.services.ScheduleCtrl;

public class ListCommandImpl implements Command {
    private final ScheduleCtrl scheduler;
    public  ListCommandImpl(ScheduleCtrl scheduler) {
        this.scheduler = scheduler;
    }

    public void execute() {
        List<SchedulerListEntry> t = this.scheduler.list();

        System.out.print("\n\n");
        System.out.println("Schedule ID   |  Is Paused | search attr");
        System.out.println("==================================================");
        for(SchedulerListEntry entry: t) {
            System.out.printf("%s | %s  |%s\n", entry.scheduleId, getIsPaused(entry), "");
        }
    }

    private boolean getIsPaused(SchedulerListEntry entry) {
        if (entry.entry != null) {
            return entry.entry.getInfo().getPaused();
        } else {
            return entry.desc.getSchedule().getState().isPaused();
        }
    }
}
