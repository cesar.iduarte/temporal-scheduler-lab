package schedulerapp.cmd;

public interface Command {
    public void execute();
}
