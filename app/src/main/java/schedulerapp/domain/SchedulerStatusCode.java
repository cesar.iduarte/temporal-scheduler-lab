package schedulerapp.domain;

public enum SchedulerStatusCode {
    OK,
    NOT_FOUND,
    ERROR,
    INVALID_ARGUMENT,
}
