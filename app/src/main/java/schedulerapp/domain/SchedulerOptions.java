package schedulerapp.domain;
import java.time.Duration;
import java.util.Map;

public class SchedulerOptions {
    public String EntityID; // Used for Workflow ID
    public String TaskQueue;
    public Class<?> WorkflowType;
    public Object Arguments;
    public Map<String, ?> SearchAttributes;
    public String CronString;
    public Duration Interval;
    public CalenderExpression calendarExp;

    public static class CalenderExpression {
        public String year;
        public String month;
        public String dayOfMonth;
        public String dayOfWeek;
        public String hour;
        public String minute;
        public String second;
    }
}
