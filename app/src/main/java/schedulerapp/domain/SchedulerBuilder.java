package schedulerapp.domain;

import io.temporal.serviceclient.WorkflowServiceStubs;

public interface SchedulerBuilder {
    SchedulerBuilder setNamespace(String namespace);
    SchedulerBuilder setWorkflowService(WorkflowServiceStubs service);
}
