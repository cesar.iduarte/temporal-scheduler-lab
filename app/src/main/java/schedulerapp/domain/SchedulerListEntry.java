package schedulerapp.domain;

import io.temporal.api.schedule.v1.ScheduleListEntry;
import io.temporal.client.schedules.ScheduleListDescription;

public class SchedulerListEntry {
    public String scheduleId;
    public ScheduleListEntry entry;
    public ScheduleListDescription desc;

    public SchedulerListEntry(String id, ScheduleListEntry entry, ScheduleListDescription desc) {
        this.scheduleId = id;
        this.entry = entry;
        this. desc = desc;
    }
}
