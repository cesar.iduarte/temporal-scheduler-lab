package schedulerapp.domain.Responses;
import io.grpc.StatusRuntimeException;
import io.temporal.client.schedules.ScheduleDescription;
import io.temporal.client.schedules.ScheduleHandle;
import schedulerapp.domain.SchedulerStatusCode;


public class SchedulerHandlerResponse extends SchedulerResponse {
    public ScheduleHandle handler;
    public ScheduleDescription descriptor;
    
    public SchedulerHandlerResponse(ScheduleHandle handler, ScheduleDescription desc) {
        super(SchedulerStatusCode.OK);
        this.handler = handler;
        this.descriptor = desc;
    }

     public SchedulerHandlerResponse(SchedulerStatusCode status, String error) {
        super(status, error);
    }

    public SchedulerHandlerResponse(StatusRuntimeException ex) {
        super(ex);
    }

    public static SchedulerHandlerResponse New(ScheduleHandle handler, ScheduleDescription desc) {
        return new SchedulerHandlerResponse(handler, desc);
    }

    public static SchedulerHandlerResponse New(Exception ex) {
        return new SchedulerHandlerResponse(SchedulerStatusCode.ERROR, ex.getMessage());
    }

    public static SchedulerHandlerResponse New(StatusRuntimeException error) {
        return new SchedulerHandlerResponse(error);
    }
}