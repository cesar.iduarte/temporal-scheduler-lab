package schedulerapp.domain.Responses;

import io.grpc.StatusRuntimeException;
import schedulerapp.domain.SchedulerStatusCode;
import io.grpc.Status;

public class SchedulerResponse {
    public SchedulerStatusCode status;
    public String scheduleID;
    public String error;


    public SchedulerResponse(SchedulerStatusCode status) {
        this.status = status;
    }

    public SchedulerResponse(String scheduleID) {
        this.status = SchedulerStatusCode.OK;
        this.scheduleID = scheduleID;
    }

    public SchedulerResponse(SchedulerStatusCode status, String error) {
        this.status = status;
        this.error = error;
    }

    public SchedulerResponse(StatusRuntimeException ex) {
        this.status = SchedulerStatusCode.ERROR;
        if (ex.getStatus() == Status.NOT_FOUND) {
            status = SchedulerStatusCode.NOT_FOUND;
        }

        this.error = ex.getMessage();
    }

    public static SchedulerResponse New(SchedulerStatusCode status) {
        return new SchedulerResponse(status);
    }

    public static SchedulerResponse New(String scheduleID) {
        return new SchedulerResponse(scheduleID);
    }

    public static SchedulerResponse New(StatusRuntimeException ex) {
        return new SchedulerResponse(ex);
    }

    public static SchedulerResponse New(Exception ex) {
        return new SchedulerResponse(SchedulerStatusCode.ERROR, ex.getMessage());
    }

    public static SchedulerResponse New(SchedulerStatusCode status, String err) {
        return new SchedulerResponse(SchedulerStatusCode.ERROR, err);
    }
}
