package schedulerapp.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Throwables;

import io.grpc.StatusRuntimeException;
import io.temporal.client.WorkflowOptions;
import io.temporal.client.schedules.Schedule;
import io.temporal.client.schedules.ScheduleActionStartWorkflow;
import io.temporal.client.schedules.ScheduleAlreadyRunningException;
import io.temporal.client.schedules.ScheduleCalendarSpec;
import io.temporal.client.schedules.ScheduleClient;
import io.temporal.client.schedules.ScheduleClientOptions;
import io.temporal.client.schedules.ScheduleDescription;
import io.temporal.client.schedules.ScheduleHandle;
import io.temporal.client.schedules.ScheduleIntervalSpec;
import io.temporal.client.schedules.ScheduleListDescription;
import io.temporal.client.schedules.ScheduleOptions;
import io.temporal.client.schedules.ScheduleRange;
import io.temporal.client.schedules.ScheduleSpec;
import io.temporal.client.schedules.ScheduleState;
import io.temporal.client.schedules.ScheduleUpdate;
import io.temporal.client.schedules.ScheduleUpdateInput;
import io.temporal.serviceclient.WorkflowServiceStubs;
import schedulerapp.domain.SchedulerBuilder;
import schedulerapp.domain.SchedulerListEntry;
import schedulerapp.domain.SchedulerOptions;
import schedulerapp.domain.SchedulerStatusCode;
import schedulerapp.domain.Responses.SchedulerHandlerResponse;
import schedulerapp.domain.Responses.SchedulerResponse;

public class ScheduleCtrlImplV2 implements ScheduleCtrl {

    private final WorkflowServiceStubs service;
    private final ScheduleClient scheduleClient;

    public ScheduleCtrlImplV2(ScheduleCtrlV2Builder builder) {
        this.service = builder.service;

        // Configure the schedule client
        // Set custom namespace
        ScheduleClientOptions options = ScheduleClientOptions.newBuilder()
                .setNamespace(builder.namespace)
                .build();

        this.scheduleClient = ScheduleClient.newInstance(this.service, options);
    }

    @Override
    public SchedulerResponse create(String scheduleID, SchedulerOptions options) {

        /*
         * Create the action that will be run when the schedule is triggered.
         */
        ScheduleActionStartWorkflow action = Utils.buildScheduleAction(options);

        // Define Spec
        ScheduleSpec spec = Utils.buildScheduleSpec(options);

        // Define the schedule we want to create
        Schedule schedule = Schedule.newBuilder()
                .setAction(action)
                .setSpec(spec)
                .build();

        try {
            ScheduleHandle handle = this.scheduleClient.createSchedule(scheduleID, schedule,
                    parseScheduleOptions(options));
            return SchedulerResponse.New(handle.getId());
        } catch (ScheduleAlreadyRunningException ex) {
            return SchedulerResponse.New(SchedulerStatusCode.ERROR, ex.getMessage());
        } catch (IllegalArgumentException ex) {
            return SchedulerResponse.New(SchedulerStatusCode.INVALID_ARGUMENT, ex.getMessage());
        } catch (Exception ex) {
            return SchedulerResponse.New(SchedulerStatusCode.ERROR, ex.getMessage());
        }
    }

    @Override
    public SchedulerResponse resume(String scheduleID) {
        SchedulerHandlerResponse resp = getHandler(scheduleID);

        if (resp.status != SchedulerStatusCode.OK) {
            return resp;
        }

        try {
            ScheduleState state = resp.descriptor.getSchedule().getState();
            if (state.isPaused()) {
                resp.handler.unpause();
                return SchedulerResponse.New(scheduleID);
            }

            return SchedulerResponse.New(SchedulerStatusCode.ERROR, "Schedule is running");
        } catch (Exception ex) {
            return SchedulerResponse.New(ex);
        }
    }

    @Override
    public SchedulerResponse delete(String scheduleID) {
        SchedulerHandlerResponse resp = getHandler(scheduleID);

        if (resp.status != SchedulerStatusCode.OK) {
            return resp;
        }

        try {
            resp.handler.delete();
            return SchedulerResponse.New(scheduleID);
        } catch (Exception ex) {
            return SchedulerResponse.New(ex);
        }
    }

    @Override
    public SchedulerResponse pause(String scheduleID) {
        SchedulerHandlerResponse resp = getHandler(scheduleID);

        if (resp.status != SchedulerStatusCode.OK) {
            return resp;
        }

        try {
            resp.handler.pause();
            return SchedulerResponse.New(scheduleID);
        } catch (Exception ex) {
            return SchedulerResponse.New(ex);
        }
    }

    @Override
    public SchedulerResponse update(String scheduleID, SchedulerOptions options) {
        ScheduleHandle handler = this.scheduleClient.getHandle(scheduleID);
        // Update does a describe internally
        handler.update((ScheduleUpdateInput source) -> {
            ScheduleDescription sourceDesc = source.getDescription();
            ScheduleActionStartWorkflow sourceAction = (ScheduleActionStartWorkflow) sourceDesc.getSchedule()
                    .getAction();

            ScheduleActionStartWorkflow action = ScheduleActionStartWorkflow.newBuilder()
                    .setArguments(options.Arguments != null ? options.Arguments : sourceAction.getArguments())
                    .setOptions(sourceAction.getOptions())
                    .setWorkflowType(sourceAction.getWorkflowType())
                    .build();

            Schedule schedule = Schedule.newBuilder()
                    .setAction(
                            action)
                    .setSpec(
                            options.Interval != null ? Utils.buildScheduleSpec(options)
                                    : sourceDesc.getSchedule().getSpec())
                    .build();
            ScheduleUpdate update = new ScheduleUpdate(schedule);

            // return null to signify no update to perform
            return update;
        });

        return SchedulerResponse.New(scheduleID);
    }

    @Override
    public List<SchedulerListEntry> list() {
        Stream<ScheduleListDescription> scheduleList = this.scheduleClient.listSchedules();
        List<SchedulerListEntry> ret = scheduleList
                .map(entry -> {
                    SchedulerListEntry desc = new SchedulerListEntry(entry.getScheduleId(), null, entry);
                    return desc;
                })
                .collect(Collectors.toList());

        return ret;
    }

    private SchedulerHandlerResponse getHandler(String scheduleID) {
        try {
            ScheduleHandle handler = this.scheduleClient.getHandle(scheduleID);
            ScheduleDescription desc = handler.describe(); // try to get ScheduleDescription Object
            return SchedulerHandlerResponse.New(handler, desc);
        } catch (StatusRuntimeException ex) {
            return SchedulerHandlerResponse.New(ex);
        } catch(Exception ex) {
            Throwable cause = Throwables.getRootCause(ex);
            
            if (cause instanceof StatusRuntimeException) {
                return SchedulerHandlerResponse.New((Exception)cause);
            }

            return SchedulerHandlerResponse.New(ex);
        }
    }

    protected ScheduleOptions parseScheduleOptions(SchedulerOptions opts) {
        ScheduleOptions.Builder factory = ScheduleOptions.newBuilder();

        if (opts.SearchAttributes != null) {
            factory
                    .setSearchAttributes(opts.SearchAttributes);
        }

        return factory.build();
    }

    public static ScheduleCtrlV2Builder newBuilder() {
        return new ScheduleCtrlV2Builder();
    }

    public static class ScheduleCtrlV2Builder implements SchedulerBuilder {
        private WorkflowServiceStubs service;
        private String namespace;

        public ScheduleCtrlV2Builder setWorkflowService(WorkflowServiceStubs service) {
            this.service = service;
            return this;
        }

        public ScheduleCtrlV2Builder setNamespace(String namespace) {
            this.namespace = namespace;
            return this;
        }

        public ScheduleCtrl build() {
            ScheduleCtrlImplV2 ctrl = new ScheduleCtrlImplV2(this);
            return ctrl;
        }
    }

    public static class Utils {
        public static ScheduleActionStartWorkflow buildScheduleAction(SchedulerOptions options) {

            WorkflowOptions workflowOptions = WorkflowOptions
                    .newBuilder()
                    .setWorkflowId(options.EntityID)
                    .setTaskQueue(options.TaskQueue)
                    .build();

            ScheduleActionStartWorkflow action = ScheduleActionStartWorkflow.newBuilder()
                    .setWorkflowType(options.WorkflowType)
                    .setArguments(options.Arguments)
                    .setOptions(workflowOptions)
                    .build();

            return action;
        }

        public static ScheduleSpec buildScheduleSpec(SchedulerOptions opts) {
            ScheduleSpec.Builder factory = ScheduleSpec.newBuilder();

            if (opts.CronString != null) {
                factory.setCronExpressions(new ArrayList<>(List.of(opts.CronString)));
            } else if (opts.Interval != null) {
                factory.setIntervals(new ArrayList<>(List.of(new ScheduleIntervalSpec(opts.Interval))));
            } else if (opts.calendarExp != null) {
                ScheduleCalendarSpec calendar = ScheduleCalendarSpec.newBuilder()
                        .setDayOfMonth(new ArrayList<>(List.of(new ScheduleRange(2))))
                        .setHour(new ArrayList<>(List.of(new ScheduleRange(12))))
                        .build();

                factory.setCalendars(new ArrayList<>(List.of(calendar)));
            }

            return factory.build();
        }
    }
}
