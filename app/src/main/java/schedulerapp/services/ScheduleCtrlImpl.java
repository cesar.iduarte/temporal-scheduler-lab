package schedulerapp.services;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import schedulerapp.Shared;
import schedulerapp.domain.SchedulerBuilder;
import schedulerapp.domain.SchedulerListEntry;
import schedulerapp.domain.SchedulerOptions;
import schedulerapp.domain.Responses.SchedulerResponse;
import io.grpc.StatusRuntimeException;
import io.temporal.api.common.v1.Payloads;
import io.temporal.api.common.v1.WorkflowType;
import io.temporal.api.schedule.v1.CalendarSpec;
import io.temporal.api.schedule.v1.Schedule;
import io.temporal.api.schedule.v1.ScheduleAction;
import io.temporal.api.schedule.v1.ScheduleListEntry;
import io.temporal.api.schedule.v1.SchedulePatch;
import io.temporal.api.schedule.v1.ScheduleSpec;
import io.temporal.api.taskqueue.v1.TaskQueue;
import io.temporal.api.workflow.v1.NewWorkflowExecutionInfo;
import io.temporal.api.workflowservice.v1.CreateScheduleRequest;
import io.temporal.api.workflowservice.v1.CreateScheduleResponse;
import io.temporal.api.workflowservice.v1.DeleteScheduleRequest;
import io.temporal.api.workflowservice.v1.DeleteScheduleResponse;
import io.temporal.api.workflowservice.v1.ListSchedulesRequest;
import io.temporal.api.workflowservice.v1.ListSchedulesResponse;
import io.temporal.api.workflowservice.v1.PatchScheduleRequest;
import io.temporal.api.workflowservice.v1.PatchScheduleResponse;
import io.temporal.api.workflowservice.v1.UpdateScheduleRequest;
import io.temporal.api.workflowservice.v1.UpdateScheduleResponse;
import io.temporal.client.WorkflowClient;
import io.temporal.common.converter.DataConverter;
import io.temporal.serviceclient.WorkflowServiceStubs;

public class ScheduleCtrlImpl implements ScheduleCtrl  {
    private final WorkflowServiceStubs service;
    private final WorkflowClient client;
    private final String namespace;

    public ScheduleCtrlImpl(ScheduleCtrlBuilder builder) {
        this.service = builder.service;
        this.client = WorkflowClient.newInstance(this.service);
        this.namespace = builder.namespace;
    }

    @Override
    public List<SchedulerListEntry> list() {
        List<ScheduleListEntry> ret;
        // For pagination pass maximum page size and the next_page_token from the response
        ListSchedulesRequest req = ListSchedulesRequest.newBuilder()
            .setNamespace(this.namespace)
            .build();

        ListSchedulesResponse resp = this.client.getWorkflowServiceStubs()
            .blockingStub()
            .listSchedules(req);

        ret = resp.getSchedulesList();

        return ret.stream()       
            .map(entry -> {
                SchedulerListEntry dep = new SchedulerListEntry(entry.getScheduleId(), entry, null);
                return dep;
            })
            .collect(Collectors.toList());
    }

    public SchedulerResponse create(String scheduleID,  SchedulerOptions options) {
        UUID requestID = UUID.randomUUID();

        // The defined cron expression "* * * * *" means that our workflow should execute every minute.
        String cronString = "* * * * *"; // https://en.wikipedia.org/wiki/Cron
        
        // Spec Using cronString
        ScheduleSpec.Builder spec =  ScheduleSpec.newBuilder().addCronString(cronString);

        // Worflow Information for the Action
        NewWorkflowExecutionInfo workflowExecutionInfo = NewWorkflowExecutionInfo.newBuilder()
            .setWorkflowType(WorkflowType.newBuilder()
                .setName("HelloWorldWorkflow")
                .build()
                
            )
            .setInput(
                Payloads.newBuilder()
                .addPayloads(
                    DataConverter.getDefaultInstance().toPayload("test").get() // Research what to use instead of getDefaultInstance()
                )
                .build()
            )
            .setWorkflowId(requestID.toString())
            .setTaskQueue(TaskQueue.newBuilder().setName(Shared.HELLO_WORLD_TASK_QUEUE).build())
            .build();

        try {
           CreateScheduleResponse resp = client
                .getWorkflowServiceStubs()
                .blockingStub()
                .createSchedule(
                    CreateScheduleRequest.newBuilder()
                    .setScheduleId(scheduleID)
                    .setRequestId(requestID.toString()) // required
                    .setNamespace(this.namespace) // required
                    .setSchedule(
                        Schedule.newBuilder()
                            .setSpec(spec.build())
                            .setAction(
                                ScheduleAction.newBuilder()
                                    .setStartWorkflow(workflowExecutionInfo)
                                    .build())
                            .build())
                    .build()
                );
            
            return SchedulerResponse.New(scheduleID);
        } catch (StatusRuntimeException ex) {
            return SchedulerResponse.New(ex);
        }
    }

    public SchedulerResponse delete(String scheduleID) {        
        try {
            DeleteScheduleRequest req = DeleteScheduleRequest.newBuilder()
                .setScheduleId(scheduleID)
                .setNamespace(this.namespace)
                .build();

            DeleteScheduleResponse resp = client
                .getWorkflowServiceStubs()
                .blockingStub()
                .deleteSchedule(req);
            return SchedulerResponse.New(scheduleID);
        } catch(Exception ex) {
            return SchedulerResponse.New(ex);
        }
    }

    public SchedulerResponse pause(String scheduleID) {
        try {
            PatchScheduleRequest req = PatchScheduleRequest.newBuilder()
                .setScheduleId(scheduleID)
                .setNamespace(this.namespace)
                .setPatch(
                    SchedulePatch.newBuilder()
                        .setPause("Paused")
                        .build()
                    )
                .build();

            PatchScheduleResponse resp = client
            .getWorkflowServiceStubs()
            .blockingStub()
            .patchSchedule(req);
        
            return SchedulerResponse.New(scheduleID);
        } catch (Exception ex) {
            return SchedulerResponse.New(ex);
        }
    }

    public SchedulerResponse resume(String scheduleID) {
        try {
            PatchScheduleRequest req = PatchScheduleRequest.newBuilder()
                .setScheduleId(scheduleID)
                .setNamespace(this.namespace)
                .setPatch(
                    SchedulePatch.newBuilder()
                        .setUnpause("1")
                        .build()
                    )
                .build();

            PatchScheduleResponse resp = client
            .getWorkflowServiceStubs()
            .blockingStub()
            .patchSchedule(req);
            return SchedulerResponse.New(scheduleID);
        } catch(Exception ex) {
            return SchedulerResponse.New(ex);
        }
    }

    public SchedulerResponse update(String scheduleID, SchedulerOptions opts) {
        UUID requestID = UUID.randomUUID();
        CalendarSpec calendarSpec;

        try {
            NewWorkflowExecutionInfo workflowExecutionInfo = NewWorkflowExecutionInfo.newBuilder()
            .setWorkflowType(WorkflowType.newBuilder().setName("HelloWorldWorkflow").build())
            .setWorkflowId(requestID.toString())
            .setTaskQueue(TaskQueue.newBuilder().setName(Shared.HELLO_WORLD_TASK_QUEUE).build())
            .build();

            calendarSpec = CalendarSpec.newBuilder()
                .setMinute("5,10,15")
                .setHour("*")
                .setMonth("*")
                .setDayOfMonth("*")
                .setDayOfWeek("*")
                .build();

            ScheduleSpec.Builder spec =  ScheduleSpec.newBuilder()
                .addCalendar(calendarSpec);

            Schedule newSchedule = Schedule.newBuilder()
                .setSpec(
                    spec.build()
                )
                .setAction(ScheduleAction.newBuilder()
                    .setStartWorkflow(workflowExecutionInfo)
                    .build()
                )
                .build();

            UpdateScheduleRequest req = UpdateScheduleRequest.newBuilder()
                .setScheduleId(scheduleID)
                .setNamespace(this.namespace)
                .setSchedule(newSchedule)
                .build();


            UpdateScheduleResponse resp = client.getWorkflowServiceStubs()
            .blockingStub()
            .updateSchedule(req);

            return SchedulerResponse.New(scheduleID);
        } catch(Exception ex) {
            return SchedulerResponse.New(ex);
        }

    }

    public static ScheduleCtrlBuilder newBuilder() {
        return new ScheduleCtrlBuilder();
    }

    public static class ScheduleCtrlBuilder implements SchedulerBuilder {
        private String namespace;
        private WorkflowServiceStubs service;
        
        public ScheduleCtrlBuilder setNamespace(String namespace) {
            this.namespace = namespace;
            return this;
        }

        public ScheduleCtrlBuilder setWorkflowService(WorkflowServiceStubs service) {
            this.service = service;
            return this;
        }

        public ScheduleCtrlImpl build() {
            ScheduleCtrlImpl ctrl = new ScheduleCtrlImpl(this);
            return ctrl;
        }
    }

}
