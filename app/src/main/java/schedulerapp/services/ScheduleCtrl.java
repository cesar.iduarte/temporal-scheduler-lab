package schedulerapp.services;

import java.util.List;

import schedulerapp.domain.SchedulerListEntry;
import schedulerapp.domain.SchedulerOptions;
import schedulerapp.domain.Responses.SchedulerResponse;

public interface ScheduleCtrl {
    public SchedulerResponse create(String scheduleID, SchedulerOptions options);
    public SchedulerResponse resume(String scheduleID);
    public SchedulerResponse delete(String scheduleID);
    public SchedulerResponse pause(String scheduleID);
    public SchedulerResponse update(String scheduleID, SchedulerOptions options);
    public List<SchedulerListEntry> list();
}