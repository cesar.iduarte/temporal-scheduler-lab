package schedulerapp;

import io.temporal.serviceclient.WorkflowServiceStubs;
import schedulerapp.cmd.Command;
import schedulerapp.cmd.CreateCommandWithCalendarExpImpl;
import schedulerapp.cmd.CreateCommandWithCronStringImpl;
import schedulerapp.cmd.CreateCommandWithIntervalImpl;
import schedulerapp.cmd.DeleteCommandImpl;
import schedulerapp.cmd.ListCommandImpl;
import schedulerapp.cmd.PausedCommandImpl;
import schedulerapp.cmd.ResumeCommandImpl;
import schedulerapp.cmd.UpdateCommandImpl;
import schedulerapp.services.ScheduleCtrl;
import schedulerapp.services.ScheduleCtrlImpl;
import schedulerapp.services.ScheduleCtrlImplV2;

/**
 * Scheduler POC!
 *
 */
public class App
{
    public static ScheduleCtrl schedulerService;
    public static void main( String[] args )
    {
        String selection = "";
        Command cmd;
        buildSchedulerV2();
       
        while(!selection.equals("e")) {
            printMenu();
            selection =  System.console().readLine();
            cmd = getCommand(selection);
            if (cmd != null) {
                cmd.execute();
                waitForEnter();
            }
        }
    }

    public static Command getCommand(String selection) {
        switch(selection.trim()) {
            case "1":
                return new CreateCommandWithIntervalImpl(schedulerService);
            case "2":
                return new CreateCommandWithCronStringImpl(schedulerService);
            case "3":
                return new CreateCommandWithCalendarExpImpl(schedulerService);
            case "4":
                return new DeleteCommandImpl(schedulerService);
            case "5":
                return new PausedCommandImpl(schedulerService);
            case "6":
                return new ResumeCommandImpl(schedulerService);
            case "7":
                return new UpdateCommandImpl(schedulerService);
            case "8":
                return new ListCommandImpl(schedulerService);
            default:
                return null;
        }
    }

    public static void printMenu() {
        System.out.println("====================================");
        System.out.println("Scheduler POC Menu");
        System.out.println("====================================");
        System.out.println("1. Create Schedule with Interval.");
        System.out.println("2. Create Schedule with Cron string.");
        System.out.println("3. Create Schedule with Calendar Expression.");
        System.out.println("4. Delete Schedule.");
        System.out.println("5. Pause Schedule.");
        System.out.println("6. Resume Schedule.");
        System.out.println("7. Update Schedule.");
        System.out.println("8. List Schedules.");
        System.out.println("e. Exit.");
    }

    public static void waitForEnter() {
        System.out.println("\nPress <Enter> to continue...\n");
        System.console().readLine();
    }

    public static void buildSchedulerV1() {
        // Low level grpc API
        App.schedulerService = ScheduleCtrlImpl.newBuilder()
            .setWorkflowService(WorkflowServiceStubs.newLocalServiceStubs()) // Very expensive operation
            .setNamespace("default") // Note: This can be set when building the WorflowService Stub above.
            .build();
    }

    public static void buildSchedulerV2() {
        // High level API
        App.schedulerService = ScheduleCtrlImplV2.newBuilder()
            .setWorkflowService(WorkflowServiceStubs.newLocalServiceStubs())
            .setNamespace("default")
            .build();
    }
}
