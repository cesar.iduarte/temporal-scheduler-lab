# About the project

POC project to work with the temporal Schedule API


# Getting started
### Prerequisites
* Temporal cluster


# Usage
## Clone the project

## Build the app
Once the project is cloned,  build the app by running the following command:

```cd app```

```mvn package```
## Run the app
For running the generated JAR file run the following command:

```java -jar target/app-1.0-SNAPSHOT.jar```